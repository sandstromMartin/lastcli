import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        ArrayList<Location> listOfLocations = new ArrayList<Location>();

        listOfLocations.add(new Location("Åmål"));
        listOfLocations.add(new Location("Emmaboda"));
        listOfLocations.add(new Location("Vedeslöv"));
        listOfLocations.add(new Location("Osby"));

        System.out.println("My favorites summer locations: ");
        for (Location l : listOfLocations){
            System.out.println(l.name);
        }


    }
}
